Files in this directory were copied from Wikipedia and we assume that using them
here qualifies as fair use under the copyright law of the United States:

https://en.wikipedia.org/wiki/File:Murder_on_the_Orient_Express_First_Edition_Cover_1934.jpg
https://en.wikipedia.org/wiki/File:StormIsland-EyeOfTheNeedle.jpg
https://en.wikipedia.org/wiki/File:And_Then_There_Were_None_First_Edition_Cover_1939.jpg
