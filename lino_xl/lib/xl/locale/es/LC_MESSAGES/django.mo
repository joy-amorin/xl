��    <      �  S   �      (  
   )     4     F     Y     l     u     �     �     �     �     �     �     �       	     	        #     +     8     F  	   O     Y  	   f     p  	   �     �     �     �     �     �     �     �     �  	               
   -     8     D     U     ^     m     {     �     �     �     �     �     �     �     �     �     �       k     
   �     �     �     �  �  �     1
     >
     O
     b
     u
     �
     �
     �
     �
     �
     �
          #  	   8     B  
   J     U     ^     r  	   �     �     �     �     �  	   �     �     �          "     3     E     M     a     x     �     �     �     �     �     �     �     �                    +     <  	   U     _     f     �     �     �     �     �     �     �                        #      ,   ;       %   +   -              (   &   "      4               /   
          9   2                    :   <                1   	      .   '         6                                 $           !             )   0      5                   3          *           8   7               Accounting Accounting Report Accounting Reports Accounting periods Accounts Analytic accounts balances Bank Statements Bank accounts Common accounts Common sheet items Contact detail types Contact details Contact persons Contacts Countries Creditors Debtors Due invoices Excerpt Types Excerpts Financial Fiscal years Flatrates Follow-up rules Functions General account balances Invoicing plans Invoicing tasks Journal Entries Journal groups Journals Legal forms Match rules Movements My invoicing plan Organizations Paper type Paper types Partner balances Partners Payment Orders Payment terms Persons Places Price factors Price rules Product Categories Products Sales Sales invoice items Sales invoices Sales rules Sheet item entries Sheet items The end user who is asking for help. This may be an external person who is not registered as a system user. Trade type Trade types Voucher types Vouchers Project-Id-Version: lino 1.6.14
Report-Msgid-Bugs-To: EMAIL@ADDRESS
PO-Revision-Date: 2024-05-14 16:18-0300
Last-Translator: Luc Saffre <luc.saffre@gmx.net>
Language-Team: es <LL@li.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Generated-By: Babel 2.12.1
X-Generator: Poedit 3.0.1
 Contabilidad Informe contable Informes contables Periodos contables Contabilidad Balances de cuentas analíticas Extractos Bancarios Cuentas bancarias Cuentas comunes Elementos comunes del balance Tipos de datos de contacto Datos de contacto Personas de contacto Contactos Países Acreedores Deudores Facturas pendientes Tipos de Extractos Extractos Finanzas Años fiscales Tarifas planas Normas de seguimiento Funciones Balances de cuentas generales Planes de facturación Tareas de facturación Entradas Diarias Grupos de diarios Diarios Formularios legales Reglas de comparación Movimientos Mi plan de facturación Organizaciones Tipo de papel Tipos de papel Balances de socios Socios Órdenes de Pago Condiciones de pago Personas Lugares Factores de precio Normas de precio Categorías de productos Productos Ventas Artículos de factura de venta Facturas de ventas Normas de venta Entrada de elementos de hoja Elementos de hoja f Tipo de operación Tipos de operación Tipos de vale Vales 